const { checkLength, checkAlphabet, checkSymbol, checkPassword, checkDigit } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has number 0-9 in password', () => {
    expect(checkDigit('bank@1123')).toBe(true)
  })
})
describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should password xxxxxxxx to be true', () => {
    expect(checkPassword('Bank@4')).toBe(false)
  })
})
